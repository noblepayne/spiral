(ns spiral.core
  (:gen-class)
  (:require [clojure.spec.alpha :as s]
            [clojure.string :as str]))

(defn make-matrix [[n m]]
  (let [coords (for [x (range n)
                     y (range m)]
                 [x y])]
    (->> coords
         (partition m)
         (mapv vec))))

(defn print-m [m]
  (println
   (str/join "\n" m)))

(defn print-matrix [dim]
  (->> dim
       make-matrix
       print-m))


(defn minus-two [n] (- n 2))

(defn make-rings [dim]
  (for [dim (iterate #(map minus-two %) dim)
        :while (every? pos? dim)]
    dim))


(defn take-step [start dirvec stepsize]
  (let [scaled-dirvec (map #(* stepsize %) dirvec)]
    (map + start scaled-dirvec)))

(defn take-steps [start dir numsteps]
  (for [stepsize (range (inc numsteps))]
    (take-step start dir stepsize)))

(defn compute-ring-size [[n m]]
  (if (or (= n 1) (= m 1))
    (* n m)
    (+ (* 2 n)
       (* 2 (- m 2)))))

(defn compute-ring-coords [[n m :as ring]]
  (cond
    (= n m 1) '([0 0])
    (= n 1)   (take-steps [0 0] [0 1] (dec m))
    (= m 1)   (take-steps [0 0] [1 0] (dec n))
    (= n m 2) '([0 0] [0 1] [1 1] [1 0])
    (= n 2)   (concat (take-steps [0 0] [0 1] (dec m))
                      (take-steps [1 (dec m)] [0 -1] (dec m)))
    (= m 2)   (concat '([0 0])
                      (take-steps [0 1] [1 0] (dec n))
                      (take-steps [(dec n) 0] [-1 0] (dec (dec n))))
    :else     (concat (take-steps [0 0] [0 1] (dec (dec m)))
                      (take-steps [0 (dec m)] [1 0] (dec (dec n)))
                      (take-steps [(dec n) (dec m)] [0 -1] (dec (dec m)))
                      (take-steps [(dec n) 0] [-1 0] (dec (dec n))))))

(defn scale-coords [scale-factor coords]
  (let [scale-vec [scale-factor scale-factor]
        scale-fn  (fn [coord] (map + coord scale-vec))]
    (map scale-fn coords)))


(defn spiral-coords [dim]
  (->> dim
       make-rings
       (map compute-ring-coords)
       (map-indexed scale-coords)
       (reduce #(into %1 %2) [])))
